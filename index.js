const AWS = require('aws-sdk')
const dynamodb = new AWS.DynamoDB({apiVersion: '2012-08-10'})

exports.handler = (event, context, callback) => {
    const playlists = event.body.playlists
    const channelId = event.query.channelid

    const putData = (c, p) => {
        var params = {
            TableName: 'playlists',
            Item: {
                'channelId' : {S: c},
                'playlistIds' : {S: JSON.stringify(p)}
            }
        }

        dynamodb.putItem(params, (err, data) => {
            if (err) {
                const response = {
                    statusCode: 500,
                    body: err,
                }
                
                callback(response, null)
            }
            else {
                const response = {
                    statusCode: 200,
                    body: 'success',
                }
                
                callback(null, response)
            }
        })
    }

    putData(channelId, playlists)
}